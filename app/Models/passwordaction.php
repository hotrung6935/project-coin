<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class passwordaction extends Model
{
    use HasFactory;

    protected $table = 'passwordAction';

    protected $fillable = [
        'hash',
        'codexn',
     ];
}
